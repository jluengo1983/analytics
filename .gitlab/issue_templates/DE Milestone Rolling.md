
# Steps to roll Milestones

[Handbook Entry](https://about.gitlab.com/handbook/business-ops/data-team/#milestone-planning)

- [ ] Close Current Milestone
- [ ] Remove `(current)` from closed milestone
- [ ] Update New Milestone with `(current)` at end of name
- [ ] Add `missed milestone` label to all issues that are rolling over 
  - Use comments on this issue to refer to any unfinished conversation on what should(n't) be rolled
- [ ] Update the [Data Eng - Current Milestone](https://gitlab.com/groups/gitlab-data/-/boards/1373923) board to point at the starting Milestone
- [ ] [Create Issue](https://gitlab.com/gitlab-data/analytics/issues/new?issuable_template=de_milestone_rolling) for next milestone rolling with Due Date set.
