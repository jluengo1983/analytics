# `YY.MM.DD - DA` Planning 

SSOT for `YY.MM.DD` work: [Data Analytics board](https://gitlab.com/groups/gitlab-data/-/boards/1587810?scope=all&utf8=%E2%9C%93&state=opened&milestone_title=YY.MM.DD%20-%20DA%20(current)). 

Please bookmark and use this board to work from in priority order. 

This issue should be opened at:
- the earliest on the Thursday before the milestone. 
- the latest on the Monday before the milestone. 

## Announcements
This section contains information on events that could affect several members of the team and the capacity. (Ex: company conferences, world events, etc...)

## Capacity :weight_lifter: 

How is `capacity` calculated? 
* Identify number of days (decimal - divide by 8)
* Convert days to weights 
* Divide by total weights of issues 


### Available Time :calendar: 
Please add a number of days (decimal) you'll be working this milestone. 
Guidelines: 
1. Each milestone has 10 days. 
1. Remove days/hours for leave: holiday, conference, etc... 
1. Remove two day for data triage, data team meetings, grooming issues, and planning. 
    - If you have multiple data triage days, please remove an additional day per triage day. 
1. Remove hours based on any additional meetings. 

<!-- in alphabetical order -->
- @derekatwood:
- @eli_kastelein: 
- @iweeks: 
- @jeanpeguero:
- @kathleentam: 
- @ken_aguilar:  
- @mpeychet\_: 
- @pluthra: 


**Total Engineering Days**: 

### Velocity :race_car: 

Average velocity (based on three recent representative milestones): 26 

Last Milestone: <!-- link here : https://gitlab.com/groups/gitlab-data/-/milestones/#-->
* **Total weight completed:** 
* **Capacity:** 
* **Velocity:** 


### Weight Available: :crystal_ball: 
Current Milestone:
* **Eng Days:** 
* **Velocity:** 

**Engineering capacity for this milestone:**

---

## Prioritized Issues :8ball: 

This section is for adding issues that have been prioritized by either the data team or function groups in priority order. 
Prioritization should be discussed in the comments. 

## OKRs :dart: 
1. <!-- link here --> 
    - <!-- issue link here --> 

## Function Groups :two_women_holding_hands: 

### G&A 
#### Projects / Working Groups 
1. <!-- project / working group name --> 
    - <!-- link here --> 

#### Ad-hoc  
##### People 
1. <!-- link here --> 

##### Finance 
1. <!-- link here --> 


### GTM 
#### Projects / Working Groups 
1. <!-- project / working group name --> 
    - <!-- link here --> 

#### Ad-hoc  
##### Sales 
1. <!-- link here --> 

##### Marketing 
1. <!-- link here --> 

##### Finance 
1. <!-- link here --> 


### R&D 
#### Projects / Working Groups 
1. <!-- project / working group name --> 
    - <!-- link here --> 

#### Ad-hoc  
##### Product 
1. <!-- link here --> 

##### Engineering 
1. <!-- link here --> 

##### Product Strategy 
1. <!-- link here --> 



---

## Planning Tasks
* [ ] Manager, Data (Analytics) creates milestone planning issue
* [ ] Team updates holidays section
* [ ] Engineers update capacity section
* [ ] Team highlights issues in `Prioritized Issues` sections 
* [ ] Manager, Data (Analytics) describes how this milestone aligns with our team's OKRs
* [ ] Manager, Data (Analytics) moves all work into `~workflow::planning breakdown`
* [ ] Manager, Data (Analytics) assigns priority by using the scoped `~P1/P2/P3/P4` labels
* [ ] Engineers estimate issue and add weights in `~workflow::planning breakdown` then move to `~workflow::scheduling`
* [ ] Engineers create milestone commitment by tagging issues in `~workflow::scheduling` with the current milestone until milestone capacity is reached and milestone commitment is made. 
* [ ] Engineers move issues into `~workflow::ready for development`.
* [ ] Engineers begin working on issues in `~workflow::ready for development`
* [ ] Manager, Data (Analytics) cleans up previous milestone planning issue (fix board, fills out content for retrospective)
* [ ] Manager, Data (Analytics) assigns team to add notes to `retrospective` in previous milestone planning issue. 
* [ ] Manager, Data (Analytics) closes out previous milestone planning issue. 


---

## Retrospective (to be completed during Data Ops before the next milestone)

[Milestone Retrospectives](https://www.scrum.org/resources/what-is-a-sprint-retrospective) are important because they allow for an opportunity to reflect on the work that was done - celebrating the success - while identifying areas of improvement. 
By the end of the retrospective, the team should identify improvements to commit to in the next milestone to improve their experience through the milestones.  

### What went well in the milestone 

### What could be improved 

### What we commit to improve in the next milestone 


---
<!-- DO NOT EDIT BELOW THIS LINE -->
/label ~Housekeeping ~Analytics ~"Data Team" ~Documentation
