
<!-- link to the milestone this issue represents (it will probably be the one following the current DE Milestone)-->
<!-- please link preceding milestone planning issue in the Linked Issues section below the description -->

# Engineering Days :calendar: 
Please add a number for days you'll be working this milestone. Engineers should reserve a day for each triage day and a day subtracted for meetings. Any time-off should also be subtracted from this number. This means that the maximum Engineering Days a Data Engineer will ever post is 8. If you have two triage days it's 7. **This section should be filled out by the data engineers themselves.**

- @jjstark: 
- @msendal: 
- @m_walker: 
- `@tayloramurphy: 0`
- @paul_armstrong:

**Total engineering days**: <!-- sum the above engineering days -->


---

# Prioritized Issues :8ball: 

This section is for adding issues that have been prioritized by either the data team or function groups. Prioritization should be discussed in the comments. **This section should be filled out by data engineers.** If you would like to have an issue added to this section please comment on this issue and tag the data engineer you work with. If you're not sure which data engineer you're working with you can tag @jjstark. 

*this section is not meant to be a complete list of issues that will be completed or addressed on a milestone, but rather represent those issues that are most important and that warrant the awareness of the data engineering team and leadership.* 

## OKR Issues :dart: 
1. <!-- link here --> 

## Function Groups :two_women_holding_hands: 
1. <!-- link here -->

---

# Velocity and Capacity :race_car: 
We calculate the velocity of our last milestone by dividing the points/weight completed by the number of engineering days that were available. **This section should be filled out by the data engineering team manager or whoever is running the planning**

## Last Milestone
Last Milestone: <!-- link here (it will probably be the current milestone) -->
Last Milestone Issue: <!-- link here and also relate it in the linked issues section below-->
* **Last milestone weight completed:** <!-- from 'Last Milestone' linked above -->
* **Last milestone engineering days:** <!-- from 'Last Milestone Issue' linked above -->
* **Last milestone velocity:** <!-- 'Last milestone weight completed' / 'Last milestone engineering days' -->


## This (starting) Milestone: :crystal_ball: 
We assume that preceding milestone's velocity is a good predictor of the following milestone's velocity. We then make sure that we have adequate capacity to address all of the **prioritized issues**. If we don't have enough capacity then we remove the issues of least priority. 

**Engineering capacity for this milestone:** <!-- Total engineering days * Last milestone velocity -->


---
<!-- DO NOT EDIT BELOW THIS LINE -->
/label ~Housekeeping ~Infrastructure ~Planning
